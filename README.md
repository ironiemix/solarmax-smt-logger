# solarmax-smt-logger

A small python script to gather information from Solarmax SMT inverters via modbus-tcp

Tested with 

* Solarmax 6SMT (2 PV Strings) 

## Installation

* Clone the repository and change into its directory
* Create a virtual environment: `python3 -mvenv venv` and activate it `source venv/bin/activate`
* Update `pip` and `wheel`: 
  * `pip install --upgrade pip`
  * `pip install wheel`
* Install requirements: `pip install -r requirements.txt`
* Edit the `base` section of `config.ini`. You have to adjust the target IP, for testing purposes you can set the debuglevel to `DEBUG`.

## Running

For now, the gathered values can only be printed to the terminal with the following command:

```
./solarmax-smt-logger.py --config config.ini -p
Spannung     String 1 395.5V
Stromstärke  String 1 6.78A
DC Leistung  String 1 2682.2W
Spannung     String 2 0.0V
Stromstärke  String 2 0.0A
DC Leistung  String 2 0.0W
Inverter Temperatur . 49.0°C
AC Leistung aktuell . 2476.8W
Energie heute ....... 8.0kWh
Maximalleistung heute 4124.0W
Energie gesamt ...... 4664.0kWh
```

## Credits

This script is heavily inspired by https://github.com/mvo5/solarmax-smtm all adresses and factors are taken from there.
However, I found https://github.com/mvo5/solarmax-smt a little too compliocated for the task at hand, and it often crashed because it connects to the modbus seperately for every register it reads. For me, this wasn't very reliable, the inverter nearly always ran into a timeout. 

This rewrite is more beginner fliendly, as I am new to python ;) and it reads all registers with one single modbus call extracting the values afterwards out of the gathered data.

