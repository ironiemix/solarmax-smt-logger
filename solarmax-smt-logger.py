#!/usr/bin/env python3
import argparse
import socket
import configparser
import paho.mqtt.client as mqtt
import pymodbus.client as ModbusClient
from pymodbus import (
    ExceptionResponse,
    ModbusException,
    pymodbus_apply_logging_config,
)

class dataregister:
    def __init__(self, name, offset, unit, length, factor, publish, subtopic):
        self.name = name
        self.offset = offset
        self.unit = unit
        self.length = length
        self.factor = factor
        self.publish = publish
        self.subtopic = subtopic

def HostUp(hostname, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex((hostname,port))
    if result == 0:
        HOST_UP=1
    else:
        HOST_UP=0
    sock.close()
    return HOST_UP

def getValues(
        host, 
        port, 
        debuglevel,
        reg_start,
        reg_length,
        cli_print,
        conf_mqtt,
        reg_list
    ):
    # activate debugging
    pymodbus_apply_logging_config(debuglevel)

    if not HostUp(host,int(port)):
        return

    client = ModbusClient.ModbusTcpClient(
        host,
        port=port,
        # timeout=10,
        # retries=3,
        # retry_on_empty=False,y
        # source_address=("localhost", 0),
    )

    client.connect()

    # Getting 45 registers, starting at address 4112
    try:
        rr = client.read_holding_registers(reg_start, reg_length, slave=1)
    except ModbusException as exc:
        print(f"Received ModbusException({exc}) from library")
        client.close()
        return
    if rr.isError():
        print(f"Received Modbus library error({rr})")
        client.close()
        return
    if isinstance(rr, ExceptionResponse):
        print(f"Received Modbus library exception ({rr})")
        # THIS IS NOT A PYTHON EXCEPTION, but a valid modbus message
        client.close()

    client.close()
    if int(conf_mqtt['enable']):
        mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
        mqttc.connect(conf_mqtt['broker'], 1883, 60)
        base_topic=conf_mqtt['base_topic']
    
    for reg in reg_list:
        if reg.length == 1:
            reg_value = rr.registers[reg.offset]
        if reg.length == 2:
            reg_value = rr.registers[reg.offset] * 65535 + rr.registers[reg.offset+1]
        if cli_print:
            print(  reg.name + " " +
                    str(reg_value/reg.factor) + 
                    reg.unit
                 )
        if reg.publish and int(conf_mqtt['enable']): 
            topic=base_topic + "/" + reg.subtopic
            payload=reg_value/reg.factor
            mqttc.publish(topic,payload,qos=1)
    

if __name__ == "__main__":
    # get cli arguments
    parser = argparse.ArgumentParser(description='Get stats from Solarmax 6/8/10/15SMT Inverters.')
    parser.add_argument(
        '-c',
        '--config',  
        help='The config file to use.'
    )
    parser.add_argument(
        '-p',
        '--print',  
        help='Print registervalues to the console.',
        action="store_true"
    )
    args = parser.parse_args()

    # read config file
    config = configparser.ConfigParser()
    config.read(args.config)
    
    # initialize register list according to config
    register_list = []
    for sec in config.sections(): 
        if(sec.startswith('register')):
            register_list.append(
                dataregister(
                    config[sec]['name'],
                    int(config[sec]['offset']),
                    config[sec]['unit'],
                    int(config[sec]['length']),
                    int(config[sec]['factor']),
                    int(config[sec]['publish']),
                    config[sec]['subtopic']
                )
            )
    

    getValues(  config['base']['smhost'],
                config['base']['smport'],
                config['base']['debuglevel'],
                int(config['base']['reg_baseaddress']),
                int(config['base']['reg_readlength']),
                args.print,
                config['mqtt'],
                register_list
             )

